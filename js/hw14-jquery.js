



jQuery(document).ready(function($){

    // переход по ссылкам-якорям
    $("body").on('click', '[href*="#"]', function(e){
        let fixed_offset = 100;
        $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1600);
        e.preventDefault();
    });



   /// to the moon button
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('.to-top-button').fadeIn();
            } else {
                $('.to-top-button').fadeOut();
            }
        });

        $('.to-top-button').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });


    //accordeon

    $(".secondary-title.post-first-title").click(function(){
        // console.log('$(this)' + $(this).id);
        $(".posts-section").slideToggle(500);
        return false;
    });


    $(".secondary-title.client-first-title").click(function(){
        // console.log('$(this)' + $(this).id);
        $(".clients").slideToggle(500);
        return false;
    });

    $(".secondary-title.top-rated").click(function(){
        // console.log('$(this)' + $(this).id);
        $(".top-rated-fotos-grid-container").slideToggle(500);
        return false;
    });

    $(".secondary-title.hot-news-title").click(function(){
        // console.log('$(this)' + $(this).id);
        $(".hot-news").slideToggle(500);
        return false;
    });




});



